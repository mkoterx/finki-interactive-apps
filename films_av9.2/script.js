$(function () {

    var $titleLabel = $('#name');
    var $yearLabel = $('#year');
    var $idLabel = $('#id');
    var $ratingLabel = $('#rating');
    var $directorLabel = $('#director');
    var $actorsLabel = $('#actors');
    var $imgPoster = $('#img-poster');

    var $dialogAddFilm = $('dialog#add-film');


    $('#table-films td:first-child').each(function () {
        setClickListener($(this));
    });

    $('#table-films').tablesorter();

    function addFilmToList(filmJSON) {
        var title = filmJSON.Title;
        var year = filmJSON.Year;
        var $row = $("<tr><td>" + title + "</td><td>" + year + "</td></tr>");
        setClickListener($row.find('td:first-child'));
        $('#table-films').append($row);
        $('#table-films').tablesorter();
    }

    function setClickListener($td) {
        $td.on('click', function () {
            var title = $td.html();
            $.ajax({
                url: 'http://omdbapi.com/?t=' + title,
                success: function (data) {
                    $titleLabel.html(data.Title);
                    $yearLabel.html(data.Year);
                    $idLabel.html(data.imdbID);
                    $ratingLabel.html(data.imdbRating);
                    $directorLabel.html(data.Director);
                    $actorsLabel.html(data.Actors);
                    $imgPoster.attr('src', data.Poster);
                    $('#film-details').css('display', 'inline-block');
                }
            });
        });
    }

    $dialogAddFilm.dialog({
        autoOpen: false,
        title: "Enter Title",
        modal: false,
        width: 300,
        show: { effect: "blind", duration: 800 },
        hide: { effect: "blind", duration: 800 },
        height: 220,
        buttons: {
            Add: function () {
                var title = $('input', this).val();
                if (title != '') {
                    $.ajax({
                        url: 'http://omdbapi.com/?t=' + title,
                        dataType: 'json',
                        success: function (data) {
                            console.log('http://omdbapi.com/?t=' + title);
                            if (data.Response == "False")
                                $('small', $dialogAddFilm).html("Movie not found!");
                            else {
                                $dialogAddFilm.dialog('close');
                                addFilmToList(data);
                            }
                        }
                    });
                }
            },
            Cancel: function () {
                $(this).dialog('close');
            }
        },
        close: function () {
            $('small', $dialogAddFilm).empty();
            $('input', $dialogAddFilm).val('');
        },
        position: {
            "my":"center top+35",
            "at":"center bottom",
            "of":$('#btn-add-film')
        }
    });

    $('input', $dialogAddFilm).on('click', function () {
        $('small', $dialogAddFilm).empty();
    });

    $('#btn-add-film').on('click', function () {
        $dialogAddFilm.dialog('open');
    });
});