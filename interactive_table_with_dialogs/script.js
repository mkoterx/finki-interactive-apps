jQuery(function() {

    var users = []; // array to hold the users
    // jQuery Objects used to pass values to the dialogs
    var $name;
    var $email;
    var $password;

    $('#users-table').tablesorter();

    /*
     ===========================================================================
     Attaching event handlers to dynamically created elements:
     $(staticAncestors).on(eventName, dynamicChild, function() {});
     ===========================================================================
     */
    $('tbody').on({
        click: function () {
            $email = $(this).parent().siblings().eq(1);
            var name = $email.prev().html();
            $("#confirm-delete-dialog label").html(name + ' ?');
            $("#confirm-delete-dialog").dialog("open");
        },
        mouseenter: function () {
            $(this).attr("src", "img/delete_active.png");
        },
        mouseleave: function () {
            $(this).attr("src", "img/delete_passive.png");
        }
    }, '.btn-del-user');

    $('tbody').on('click', '.btn-edit-user', function () {
            $name = $(this).parent().siblings().eq(0);
            $email = $(this).parent().siblings().eq(1);
            $password = $(this).parent().siblings().eq(2);
            $("#edit-user-dialog input[name='name']").val($name.html());
            $("#edit-user-dialog input[name='email']").val($email.html());
            $("#edit-user-dialog input[name='password']").val($password.html());
            $("#edit-user-dialog").dialog("open");
    });

    $('tbody').on({
        mouseenter: function () {
            $('img', $(this)).show();
        },
        mouseleave: function () {
            $('img', $(this)).hide();
        }
    }, 'tr');

    /*
     ===========================================================================
     Functions for manipulating Users
     ===========================================================================
     */
    function User(name, email, password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    // Searches by email because emails are unique
    function findUser(email) {
        for (var i = 0; i < users.length; i++)
            if (users[i].email === email)
                return users[i];
        return null;
    }

    // Searches by email because emails are unique
    function deleteUser(email) {
        for (var i = 0; i < users.length; i++)
            if (users[i].email === email)
                users.splice(i, 1);
    }

    function addUserInTable(user) {
        var $row = $("<tr><td>"+user.name+"</td><td>"+user.email+"</td><td>"+user.password+"</td></tr>");
        var $td = $("<td></td>");
        var $imgDelUser = $("<img class='btn-del-user' src='img/delete_passive.png'>");
        var $imgEditUser = $("<img class='btn-edit-user' src='img/edit_user.png'>");
        $imgDelUser.hide();
        $imgEditUser.hide();
        $td.append($imgEditUser);
        $td.append($imgDelUser);
        $row.append($td);
        $("#users-table tbody").append($row);
        $('#users-table').trigger('update');
    }

    /*
    ===========================================================================
                Creating Dialogs using jQuery UI Dialog Plugin
    ===========================================================================
    */

    // Add-User-Dialog
    $("#add-user-dialog").dialog({
        autoOpen: false,
        width: 350,
        draggable: false,
        modal: true,
        title: "Add New User",
        buttons: {
            Add: function() {
                var name = $("#add-user-dialog [name='name']").val();
                var email = $("#add-user-dialog [name='email']").val();
                var pass = $("#add-user-dialog [name='password']").val();
                var newUser = new User(name, email, pass);
                users.push(newUser);
                addUserInTable(newUser);
                $(this).dialog("close");
            },
            Cancel: function() {
                $(this).dialog("close");
            }
        },
        close: function () {
            $('input', this).val(''); // empty the form
        }
    });

    // Edit-User-Dialog
    $("#edit-user-dialog").dialog({
        autoOpen: false,
        width: 350,
        modal: true,
        title: "Edit User",
        buttons: {
            Done: function () {
                var user = findUser($email.html());

                // Get the new values from the edit-dialog
                var newName = $("#edit-user-dialog [name='name']").val();
                var newEmail = $("#edit-user-dialog [name='email']").val();
                var newPassword = $("#edit-user-dialog [name='password']").val();

                // Update user's info
                user.name = newName;
                user.email = newEmail;
                user.password = newPassword;

                // Set the new values for the current user in the table,
                // this way there's no need to build the whole table again
                $name.html(newName);
                $email.html(newEmail);
                $password.html(newPassword);

                $(this).dialog("close");
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    });

    // Confirm-Delete-User-Dialog
    $("#confirm-delete-dialog").dialog({
        width: 350,
        autoOpen: false,
        modal: true,
        title: "Delete User?",
        buttons: {
            Yes: function() {
                // searches by email because emails are unique
                var $row = $email.parent();
                deleteUser($email.html());
                $row.remove();
                $(this).dialog("close");
            },
            No: function() {
                $(this).dialog("close");
            }
        },
        close: function () {
            $('label', $(this)).html('')
        }
    });

    /*
    ===========================================================================
                        Attaching Buttons to Dialogs
    ===========================================================================
    */
    $("#btn-add-user, #btn-add-user-icon").on('click', function() {
        $("#add-user-dialog").dialog("open");
    });

    $("#btn-show-users").on('click', function() {
        $("section#users").slideToggle();
    });
});