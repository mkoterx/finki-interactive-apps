$(function () {

    $('.selectable').selectable();

    $('#btn-add').on('click', function () {
        $('#source').find('.ui-selected').appendTo('#destination').removeClass('ui-selected');
    });

    $('#btn-remove').on('click', function () {
        $('#destination').find('.ui-selected').appendTo('#source').removeClass('ui-selected');
    });

});