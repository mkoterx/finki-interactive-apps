jQuery(function () {

    $('#tabs').tabs();

    var urlCpp = "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&order=relevance&q=learning+c%2B%2B&type=video&videoCaption=closedCaption&key=AIzaSyAyvig5VkfPt_lBR4sFl-ajsULtgUHmTwA";
    var urlJava = "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&order=relevance&q=learning+java&type=video&videoCaption=closedCaption&key=AIzaSyAyvig5VkfPt_lBR4sFl-ajsULtgUHmTwA";
    var urlJquery = "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&order=relevance&q=learning+jquery&type=video&videoCaption=closedCaption&key=AIzaSyAyvig5VkfPt_lBR4sFl-ajsULtgUHmTwA";
    var $listCpp = $('#learning-cpp ul');
    var $listJava = $('#learning-java ul');
    var $listJquery = $('#learning-jquery ul');

    function getEntries(list, url) {
        jQuery.ajax({
            url: url,
            dataType: 'jsonp',
            success: function (data) {
                jQuery.each(data.items, function (index, video) {
                    var thumbnailUrl = video.snippet.thumbnails.default.url;
                    var title = video.snippet.title;
                    var publishedAt = video.snippet.publishedAt;
                    var description = video.snippet.description;
                    var $li = $('<li></li>');
                    var $div = $('<div class="item-container"></div>');
                    var $thumbnail = $('<img class="video-thumbnail" />');
                    $thumbnail.attr({
                        'src': thumbnailUrl,
                        'title': description
                    });
                    var $h4 = $('<h4 class="video-title">' + title + '</h4>');
                    var $p = $('<p class="video-published-at"><small>Published at: ' + publishedAt + '</small></p>');
                    $div.append($thumbnail);
                    $div.append($h4);
                    $div.append($p);
                    $li.append($div);
                    list.append($li);
                });
            }
        });
    }

    getEntries($listCpp, urlCpp);
    getEntries($listJava, urlJava);
    getEntries($listJquery, urlJquery);

    $('#favourite-videos ul').sortable();


    $('#learning-cpp ul, #learning-java ul, #learning-jquery ul').sortable({
        connectWith: "#favourite-videos ul"
    });

    $(document).tooltip({
        tooltipClass: 'tooltip-video',
        items: '.video-thumbnail',
        content: function () {
            return $(this).attr('title');
        },
        position: {
            my: 'left+15 center',
            at: 'right center'
        }
    });


});