$(function () {

    function Student(name, index, points) {
        this.name = name;
        this.index = index;
        this.points = points;
    }

    var students = [
        new Student('Петре Петревски', 111113, 30),
        new Student('Аце Ацевски', 111114, 90),
        new Student('Мите Митрески', 111115, 100),
        new Student('Мирко Мирковски', 111117, 85),
        new Student('Никола Николовски', 111111, 80),
        new Student('Симона Симоноска', 111112, 70),
        new Student('Ристе Ристески', 111116, 60)
    ];

    for (var i = 0; i < students.length; i++) {
        $li = $('<li title=""><input type="checkbox">' + students[i].name + '</li>');
        addTooltip($li, students[i]);
        addNoselectClass($li);
        if (i <= 3)
            $('#pre-final-list').append($li);
        else
            $('#final-list').append($li);
    }

    function addTooltip($li, student) {
        $li.attr('title', 'Индекс: ' + student.index + ' | Поени: ' + student.points);
        $li.tooltip({
            tooltipClass: "tooltip-style",
            position: {
                my: 'left-30 center',
                at: 'right center'
            }
        });
    }

    function addNoselectClass($el) {
        $el.addClass('noselect');
    }


    $('#tabs').tabs({
        beforeActivate: function (event, ui) {
            if (ui.newTab.index() == 1) {
                estimateProgress();
            }
        }
    });

    $('#dialog-add-student').dialog({
        title: 'Додади студент',
        width: 250,
        autoOpen: false,
        draggable: false,
        buttons: {
            'Ok': function () {
                var name = $('input[name="name"]', this).val();
                var index = $('input[name="index"]', this).val();
                var points = $('input[name="points"]', this).val();
                var student = new Student(name, index, points);
                students.push(student);
                $li = $('<li title=""><input type="checkbox">' + student.name + '</li>');
                addTooltip($li, student);
                addNoselectClass($li);
                $('#pre-final-list').append($li);
                $(this).dialog('close');

            },
            'Излези': function () {
                $(this).dialog('close');
            }
        },
        close: function () {
            $('input', $(this)).val('');
            $(this).dialog('close');
        },
        show: 'blind',
        hide: 'blind',
        position: {
            my: 'left+10 center+115',
            at: 'right center',
            of: '#btn-add-student'
        }
    });

    $('#btn-add-student').on('click', function () {
        $('#dialog-add-student').dialog('open');
    });


    $(document).on('click', '#students li', function () {
        if ($('input', $(this)).prop('checked') == false) {
            $('input', $(this)).prop('checked', true);
            $(this).addClass('selected');
        }
        else {
            $('input', $(this)).prop('checked', false);
            $(this).removeClass('selected');
        }
    });

    $(document).on('click', '#students input', function () {
        if ($(this).prop('checked') == false) {
            $(this).prop('checked', true);
            $(this).parent().addClass('selected');
        }
        else {
            $(this).prop('checked', false);
            $(this).parent().removeClass('selected');
        }
    });

    $('#btn-transfer-right').on('click', function () {
        $('#pre-final-list input:checked').each(function () {
            var points = $(this).parent().attr('title').split('|')[1].split(':')[1].trim();
            if (points >= 50) {
                $li = $(this).parent();
                $li.appendTo('#final-list');
                $(this).prop('checked', false);
                $(this).parent().removeClass('selected');
            } else {
                alert("Пробавте да додатете студент со помалку од 50 бода");
                $(this).prop('checked', false);
                $(this).parent().removeClass('selected');
            }
        });
    });

    $('#btn-transfer-left').on('click', function () {
        $('#final-list input:checked').each(function () {
            var points = $(this).parent().attr('title').split('|')[1].split(':')[1].trim();
            $li = $(this).parent();
            $li.appendTo('#pre-final-list');
            $(this).prop('checked', false);
            $(this).parent().removeClass('selected');
        });
    });

    $('#progress6').progressbar();
    $('#progress7').progressbar();
    $('#progress8').progressbar();
    $('#progress9').progressbar();
    $('#progress10').progressbar();

    function estimateProgress() {
        var count = $('#pre-final-list li').size() + $('#final-list li').size();
        var count6 = 0;
        var count7 = 0;
        var count8 = 0;
        var count9 = 0;
        var count10 = 0;

        $('#final-list li').each(function () {
            var points = $(this).attr('title').split('|')[1].split(':')[1].trim();
            if (points > 50 && points <= 60) count6++;
            if (points > 60 && points <= 70) count7++;
            if (points > 70 && points <= 80) count8++;
            if (points > 80 && points <= 90) count9++;
            if (points > 90 && points <= 100) count10++;
        });

        var gradeCounts = [count6, count7, count8, count9, count10];

        for (var i = 0; i < 5; i++) {
            var barValue = gradeCounts[i] / count * 100;
            var index = i + 6;
            $('#progress' + index).progressbar('value', barValue);
            $('#progress' + index + '-text').html('Оценка ' + index + ': ' + barValue.toFixed(1) + '%');
        }
    }

});