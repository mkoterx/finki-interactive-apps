$(function () {
    $('#folder img').draggable({
        revert: true
    });

    $('#trash').droppable({
        tolerance: 'touch',
        over: function (event, ui) {
            $(this).attr('src', 'img/trash2.png');
            $(ui.draggable).css('opacity', '0.5');
        },
        out: function (event, ui) {
            $(this).attr('src', 'img/trash1.png');
            $(ui.draggable).css('opacity', '1');
        },
        drop: function (event, ui) {
            $(ui.draggable).hide();
            $(this).attr('src', 'img/trash1.png');
        }
    });

    $('#restore').on('click', function () {
        $('#folder img').each(function () {
            $(this).show();
            $(this).css('opacity', '1');
        });
    });


});