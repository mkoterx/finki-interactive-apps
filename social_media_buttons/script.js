$(function () {
    // Hide all the tooltips
    $("#social li a strong").css("opacity", "0");

    $("#social li").hover(function () { // Mouse over
        $(this).siblings().stop().fadeTo(500, 0.2);
        $("a strong", this)
            .stop()
            .animate({
                opacity: 1,
                top: "-10px",
            }, 300);
    }, function () { // Mouse out
        $(this).siblings().stop().fadeTo(500, 1);
        $("a strong", this)
            .stop()
            .animate({
                opacity: 0,
                top: "-1px",
            }, 300);
    });
});