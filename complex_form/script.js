$(function () {

    var $progressbar = $("#progressbar");
    $progressbar.progressbar({value: 0});
    var $progressbar_value = $progressbar.find('.ui-progressbar-value');

    var validFields = [false, false, false, false];
    var $talkbubble = $("#talkbubble");

    /*
     ==========================================================================
     Managing the password input field
     ==========================================================================
     */
    var $input_pass = $("input[name='password']");

    // Attaching Event Handlers
    $input_pass.focusin(function () {
        $(this).css("background-color", "white");
        var strength = calculatePasswordStrength($(this).val());
        if (strength > 0 && strength < 4) {
            $("#pwd-ack").hide();
            $progressbar.css("display", "inline-block");
        }
    });

    $input_pass.focusout(function () {
        $progressbar.hide();
        var strength = calculatePasswordStrength($(this).val());
        if (strength > 0 && strength < 4) {
            $(this).css("background", "lightcoral");
            validFields[0] = false;
            var message = "Password must be at least 8 characters long and must " +
                "contain at least one letter one digit and one special character.";
            showTalkbubble(message, $(this));
        }
        else if (strength == 4) {
            $("#pwd-ack").css("display", "inline-block");
            validFields[0] = true;
        }

        checkFormValidity();
    });

    // I use the "input" event register with the .on method, because
    // keypress and keydown events are fired before the new character is
    // added to the value of the element (so the first event is fired before
    // the first character is added, while the value is still empty).
    // The "input" and "keyup" events are fired after the character has
    // been added. "keyup" has more lag, so I choose the "input" event.
    $input_pass.on("input", function () {
        var score = calculatePasswordStrength($(this).val());

        if (score == 1) $progressbar_value.css("background", "red");
        else if (score == 2) $progressbar_value.css("background", "orange");
        else if (score == 3) $progressbar_value.css("background", "yellow");
        else if (score == 4) $progressbar_value.css("background", "green");

        $progressbar.progressbar("value", score * 25);

        if (score != 4) {
            $("#pwd-ack").hide();
            $progressbar.css("display", "inline-block");
            validFields[0] = false;
        } else
            validFields[0] = true;

        var pwd1 = $(this).val();
        var pwd2 = $("#pwd-confirm").val();
        if (pwd1 != pwd2) {
            $("#pwd-confirm-ack").hide();
            validFields[1] = false;
        } else if (pwd1 != "") {
            validFields[1] = true;
            $("#pwd-confirm-ack").css("display", "inline-block");

        }

        checkFormValidity();
    });

    /* Function to calculate the strength of a given password */
    function calculatePasswordStrength(password) {
        var result = 0;
        result += /[a-zA-Z]+/.test(password) ? 1 : 0;
        result += /[0-9]+/.test(password) ? 1 : 0;
        result += /[^a-zA-Z0-9]+/.test(password) ? 1 : 0;
        result += password.length >= 8 ? 1 : 0;
        return result;
    }

    $("#pwd-confirm").on("input", function () {
        var password = $input_pass.val();
        if ($(this).val() != password || $progressbar.progressbar("value") < 100) {
            $("#pwd-confirm-ack").hide();
            validFields[1] = false;
        } else {
            $("#pwd-confirm-ack").css("display", "inline-block");
            validFields[1] = true;
        }

        checkFormValidity();
    });

    $("#pwd-confirm").on("focusout", function () {
        var password = $input_pass.val();
        var confirmed = $(this).val();
        if (confirmed != password && confirmed != "") {
            $(this).css("background-color", "lightcoral");
            var message = "Passwords don't match!";
            showTalkbubble(message, $(this));
            $("#pwd-confirm-ack").hide();
            validFields[1] = false;
        } else
            validFields[1] = true;

        checkFormValidity();
    });

    $("#pwd-confirm").on("focusin", function () {
        $(this).css("background-color", "white");
    });

    $("#fname").on("input focusout", function () {
        var fname = $(this).val();
        if (/[0-9]+/.test(fname) || /[^a-zA-Z0-9]+/.test(fname)) {
            $(this).css("background-color", "lightcoral");
            validFields[2] = false;
            $("#fname-ack").hide();
        } else if (fname != "") {
            validFields[2] = true;
            $(this).css("background-color", "white");
            $("#fname-ack").css("display", "inline-block");
        } else if (fname == "") {
            $("#fname-ack").hide();
            validFields[2] = false;
        }

        checkFormValidity();
    });

    $("#fname").on("focusin", function () {
        $(this).css("background-color", "white");
    });

    $("#lname").on("input focusout", function () {
        var lname = $(this).val();
        if (/[0-9]+/.test(lname) || /[^a-zA-Z0-9]+/.test(lname)) {
            $(this).css("background-color", "lightcoral");
            validFields[3] = false;
            $("#lname-ack").hide();
        } else if (lname != "") {
            console.log(2);
            validFields[3] = true;
            $(this).css("background-color", "white");
            $("#lname-ack").css("display", "inline-block");
        } else if (lname == "") {
            $("#lname-ack").hide();
            validFields[3] = false;
        }

        checkFormValidity();
    });

    $("#lname").on("focusin", function () {
        $(this).css("background-color", "white");
    });

    function checkFormValidity() {
        for (var i = 0; i < validFields.length; i++) {
            if (validFields[i] == false) {
                $("#fake-submit-btn").css({"display": "block", "margin": "0px auto"});
                $("#submit-btn").hide();
                return false;
            }
        }
        $("#fake-submit-btn").hide();
        $("#submit-btn").css({"display": "block", "margin": "0px auto"});
        return true;
    }

    function showTalkbubble(message, $target) {
        $talkbubble.html(message);
        $talkbubble.show();
        $talkbubble.position({
            my: "left center+15",
            at: "right+30 center",
            of: $target
        });
        setTimeout(function () {
            $talkbubble.html("");
            $talkbubble.hide();
        }, 3000);
    }

    function submitForm() {
        alert("sdfasf");
        console.log("dfassdf");
        console.log(checkFormValidity());
        if (checkFormValidity()) {
            document.submit();
            alert("Data Submited");
            return true;
        } else
            return false;
    }


});