$(function () {

    function Student(index, lastName, firstName) {
        this.index = index;
        this.firstName = firstName;
        this.lastName = lastName;
        this.partialExam1 = 0;
        this.partialExam2 = 0;
        this.projectWork = 0;
        this.getsSignature = false;
    }

    var students = [];

    function addStudent(s) {
        $row = $('<tr><td class="index">' + s.index + '</td><td class="lname">' + s.lastName + '</td><td class="fname">' +
            s.firstName + '</td><td class="signature"><input type="checkbox"></td><td class="pe1"></td><td class="pe2"></td><td class="project"></td><td class="total-points"></td>><td class="grade"></td>');
        $('#table-students').append($row);
    }

    $('#table-students').tablesorter();
    $('#table-passed-students').tablesorter();


    function addNoselectClass($el) {
        $el.addClass('noselect');
    }

    function findStudent(index) {
        for (var i = 0; i < students.length; i++)
            if (students[i].index == index)
                return students[i];
        return null;
    }


    $('#tabs').tabs({
        beforeActivate: function (uevent, ui) {
            if (ui.newTab.index() == 0) {
                $('#table-students tbody tr:not(.passed)').each(function () {
                    $(this).show();
                });
                $('.signature input').each(function () {
                    $(this).attr('disabled', false);
                });
                $('#students').append($('#table-students'));
                $(document).on('dblclick', '#table-students td:not(.signature)', dblclickFunction);
            } else if (ui.newTab.index() == 1) {
                $('#table-students tbody tr:not(.passed)').each(function () {
                    $(this).hide();
                });
                $('.signature input').each(function () {
                    $(this).attr('disabled', true);
                });
                $('#passed-students').append($('#table-students'));
                $(document).off('dblclick', '#table-students td:not(.signature)', dblclickFunction);
            } if (ui.newTab.index() == 2) {
                estimateProgress();
            }
        }
    });

    $('#dialog-add-student').dialog({
        title: 'Додади студент',
        width: 250,
        autoOpen: false,
        draggable: false,
        modal: true,
        buttons: {
            'Ok': function () {
                var index = $('input[name="index"]', this).val();
                var lname = $('input[name="lastname"]', this).val();
                var fname = $('input[name="firstname"]', this).val();
                if (index != '' && lname != '' && fname != '') {
                    var student = new Student(index, lname, fname);
                    students.push(student);
                    addStudent(student);
                    $('#table-students').trigger('update');
                    $(this).dialog('close');
                }

            },
            'Излези': function () {
                $(this).dialog('close');
            }
        },
        close: function () {
            $('input', $(this)).val('');
            $(this).dialog('close');
            console.log(students);
        },
        show: 'blind',
        hide: 'blind',
        position: {
            my: 'center top+10',
            at: 'center bottom',
            of: '#btn-add-student'
        }
    });

    $('#btn-add-student').on('click', function () {
        $('#dialog-add-student').dialog('open');
    });

    var oldIndex = '';
    $(document).on('dblclick', '#table-students td:not(.signature)', dblclickFunction);

    function dblclickFunction() {
        $(this).parent().addClass('edit-mode');

        // Disables editing more than one student at a time
        if ($('#table-students tr.edit-mode').length > 1) {
            $(this).parent().removeClass('edit-mode');
            return;
        }

        if (!$(this).hasClass('edit-mode')) {
            $(this).addClass('edit-mode');

            var oldValue = $(this).html();
            if ($(this).hasClass('index'))
                oldIndex = oldValue;
            if (!$(this).hasClass('signature'))
                $(this).html('');

            var $inputField;
            if ($(this).hasClass('fname') || $(this).hasClass('lname') || $(this).hasClass('index'))
                $inputField = $('<input type="text">');
            else if ($(this).hasClass('pe1') || $(this).hasClass('pe2') || $(this).hasClass('project'))
                $inputField = $('<input type="number" min="0" max="100">');
            else if ($(this).hasClass('project'))
                $inputField = $('<input type="number" min="0" max="20">');

            $inputField.addClass('edit-input-field');
            $inputField.val(oldValue);
            $(this).append($inputField);
            $('#btn-save-changes').show();
        }
    }

    $('#btn-save-changes').on('click', function () {
        var $row;
        $('td.edit-mode').each(function () {
            $(this).removeClass('edit-mode');
            $(this).parent().removeClass('edit-mode');
            $row = $(this).parent();
            var value = $('input', this).val();
            $('input', this).remove();
            $(this).html(value);

            var index;
            if (oldIndex != '') {
                index = oldIndex;
                oldIndex = '';
            }
            else index = $(this).siblings().eq(0).html();

            var student = findStudent(index);
            if ($(this).hasClass('index'))
                student.index = value;
            else if ($(this).hasClass('fname'))
                student.firstName = value;
            else if ($(this).hasClass('lname'))
                student.lastName = value;
            else if ($(this).hasClass('pe1'))
                student.partialExam1 = value;
            else if ($(this).hasClass('pe2'))
                student.partialExam2 = value;
            else if ($(this).hasClass('project'))
                student.projectWork = value;
        });
        $(this).hide();
        estimateGrade($row);
        isPassed($row);
        $('#table-students').trigger('update');
        console.log(students);
    });

    function isPassed($row) {
        if ($('.grade', $row).html() > 5 && $('.signature input', $row).prop('checked') == true) {
            $row.addClass('passed');
        }
        else {
            $row.removeClass('passed');
        }
    }

    $(document).on('click', '.signature input', function () {
        var index = $(this).parent().siblings().eq(0).html();
        var student = findStudent(index);

        if ($(this).prop('checked') == false)
            student.getsSignature = false;
        else
            student.getsSignature = true;

        var $row = $(this).parent().parent();
        estimateGrade($row);
        isPassed($row);
    });

    function estimateGrade($row) {
        var pe1 = $('.pe1', $row).html();
        var pe2 = $('.pe2', $row).html();
        var projectWork = parseFloat($('.project', $row).html());
        console.log(pe1 + " " + pe2 + " " + projectWork);
        if (pe1 != '' && pe2 != '' && projectWork != '') {
            if (pe1 < 40 || pe2 < 40) {
                $('.total-points', $row).html('-');
                $('.grade', $row).html(5);
            }
            else {
                var totalPoints = pe1 * 0.45 + pe2 * 0.45 + projectWork;
                $('.total-points', $row).html(totalPoints.toFixed(2));
                console.log("sdfs");
                $('.grade', $row).html(convertPointsToGrade(totalPoints));
            }
        }
    }

    function convertPointsToGrade(points) {
        if (points <= 50) return 5;
        else if (points > 50 && points <= 60) return 6;
        else if (points > 60 && points <= 70) return 7;
        else if (points > 70 && points <= 80) return 8;
        else if (points > 80 && points <= 90) return 9;
        else if (points > 90 && points <= 100) return 10;
    }


    $('#progress-passed').progressbar();
    $('#progress6').progressbar();
    $('#progress7').progressbar();
    $('#progress8').progressbar();
    $('#progress9').progressbar();
    $('#progress10').progressbar();

    function estimateProgress() {
        var count = $('#table-students tbody tr').length;
        var countPassed = $('#table-students tbody tr.passed').length;
        var count6 = 0;
        var count7 = 0;
        var count8 = 0;
        var count9 = 0;
        var count10 = 0;

        var barValue = countPassed / count * 100;
        $('#progress-passed').progressbar('value', barValue);
        $('#progress-passed-text').html('Положиле: ' + barValue.toFixed(1) + '%');

        $('#table-students tr.passed').each(function () {
            if ($('.grade', $(this)).html() == 6)
                count6++;
            if ($('.grade', $(this)).html() == 7)
                count7++;
            if ($('.grade', $(this)).html() == 8)
                count8++;
            if ($('.grade', $(this)).html() == 9)
                count9++;
            if ($('.grade', $(this)).html() == 10)
                count10++;
        });

        var gradeCounts = [count6, count7, count8, count9, count10];

        for (var i = 0; i < 5; i++) {
            barValue = gradeCounts[i] / countPassed * 100;
            var index = i + 6;
            $('#progress' + index).progressbar('value', barValue);
            $('#progress' + index + '-text').html('Оценка ' + index + ': ' + barValue.toFixed(1) + '%');
        }
    }

});