$(function(){

        $.each($("#example-form div"), function(index, item) {
          var current = $(item);
          if(index == 0) 
            $("#status").append("<span class='active'>" + current.find('h3').html() +"</span>");
          else 
            $("#status").append("<span class='pending'>" + current.find('h3').html() +"</span>");
        });

        $("#example-form div").hide();
        var curr; // keeps track of the current window
        curr = $("#example-form div").first();
        curr.show();
        
        $("#next").click(function() {
          if (!curr.is(":last-child")) {
            curr.hide();
            var next = curr.next();
            next.show();
            curr = next;
            updateStatus(next.index()); 
          } else {
            alert("DONE!");
          }
        });

        $("#prev").click(function() {
          if (!curr.is(":first-child")) {
            var prev = curr.prev();
            curr.hide();
            prev.show();
            curr = prev;
            updateStatus(prev.index());
          }
        });
     });

     function updateStatus(index){
        $("#status span").removeClass("passed active pending");
        $.each($("#status span"), function(i, item){
          var step = $(item);
          if (i < index)
            step.addClass('passed');
          else if (i == index)
            step.addClass('active');
          else
            step.addClass('pending');
        });
     }