$(function () {

    var emailValidated = false;
    var email = '';

    function isEmailValid() {
        if ($('#ins_email').is(':valid')) {
            return true;
            email = $('#ins_email').val();
        }
    }

    $('.email_insert').dialog({
        autoOpen: false,
        buttons: {
            'Ok': function () {
                if (isEmailValid()) {
                    emailValidated = true;
                    $(this).dialog('close');
                    showShareInterface();
                    $('#error').hide();
                } else {
                    $('#error').show();
                }
            },
            'Cancel': function () {
                $(this).dialog('close');
            }
        }
    });

    function showShareInterface() {
        $('#share_interface').show();
        $('#search_interface').hide();
        $('.results').hide();
    }

    $('#share_button').on('click', function () {
        if (!emailValidated)
            $('.email_insert').dialog('open');
        else
            showShareInterface();
    });

    $('#search_button').on('click', function () {
        $('#share_interface').hide();
        $('#search_interface').show();
        $('.results').show();
    });

    $('#search').on('click', function () {
        var searchTerm = $('#searchTerm').val();
        if (searchTerm != '') {
            $.ajax({
                url: 'http://www.flickr.com/services/feeds/photos_public.gne?tags=' + searchTerm + '&format=json#',
                dataType: 'jsonp',
                jsonp: 'jsoncallback',
                success: function (response) {
                    console.log('http://www.flickr.com/services/feeds/photos_public.gne?tags=' + searchTerm + '&format=json#');
                    $('.results_list').empty();
                    $.each(response.items, function (index, item) {
                        var $poster = $('<img>');
                        $poster.attr('src', item.media.m);
                        var $title = $('<span class="item-title">' + item.title + '</span>');
                        var $link = $('<a href="' + item.link + '">link</a>');
                        var $published = $('<span class="item-published">Published: ' + item.published + '</span>');
                        var $item = $('<div class="item">').append($title).append($poster).append($published).append($link);
                        $('.results_list').append($('<li class="list-item">').append($item));
                    })
                }
            })
        }
    });

    $('.results_list').sortable({
        connectWith: '.favorites_list'
    });

    $('.favorites_list').sortable({
        items: 'li:not(.unselectable)',
        connectWith: '.results_list'
    });

    $('body').on('click', '.item', function () {
        window.location = $('img', this).attr('src');
    });

    var shareLink;
    var shareMedia;
    $('.social li').on('click', function () {
        $('.selected').removeClass('selected');
        $(this).toggleClass('selected');
        shareLink = $('a', this).attr('link');
        shareMedia = $(this).attr('class').split(' ')[0];
    });

    $('.share_dialog').dialog({
        autoOpen: false
    });
    
    $('#share_dialog_call').on('click', function () {
        $('.sent-item').remove();
        $('#info-social').html(shareMedia);
        $('#info-social-link').html(shareLink);
        $('.favorites_list .list-item').each(function () {
            var title =  $('.item-title', this).html();
            $('.share_dialog').append('<span class="sent-item">' + title + '</span>');
        });
        $('.share_dialog').dialog('open');
    });

});