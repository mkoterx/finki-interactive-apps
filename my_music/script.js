$(function () {

    $('#table-music-list').tablesorter();

    jQuery.ajax({
        url: "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=PL2vrmw2gup2Jre1MK2FL72rQkzbQzFnFM&key=AIzaSyAyvig5VkfPt_lBR4sFl-ajsULtgUHmTwA",
        dataType: 'jsonp',
        success: function (data) {
            var videoIDs = '';
            jQuery.each(data.items, function (index, item) {
                videoIDs += (item.snippet.resourceId.videoId + ',');
            });
            videoIDs = videoIDs.substring(0, videoIDs.length - 1);
            console.log(videoIDs);
            jQuery.ajax({
                url: "https://www.googleapis.com/youtube/v3/videos?part=contentDetails%2Cstatistics%2Csnippet%2Cplayer&id=" +
                videoIDs + "&key=AIzaSyAyvig5VkfPt_lBR4sFl-ajsULtgUHmTwA",
                dataType: 'jsonp',
                success: function (data) {
                    console.log("https://www.googleapis.com/youtube/v3/videos?part=contentDetails%2Cstatistics%2Csnippet%2Cplayer&id=" +
                    videoIDs + "&key=AIzaSyAyvig5VkfPt_lBR4sFl-ajsULtgUHmTwA");
                    jQuery.each(data.items, function (index, item) {
                        var title = item.snippet.title;
                        var thumbnailUrl = item.snippet.thumbnails.default.url;
                        var $poster = $('<img>');
                        $poster.attr('src', thumbnailUrl);
                        var likeCount = item.statistics.likeCount;
                        var dislikeCount = item.statistics.dislikeCount;
                        var viewCount = item.statistics.viewCount;
                        $row = $('<tr><td>' + $poster[0].outerHTML + '</td><td>' + title + '</td><td>' + likeCount + '</td><td>' +
                            dislikeCount + '</td><td>' + viewCount + '</td></tr>');
                        $row.appendTo('#table-music-list');
                        // setEditInPlace($('td:nth-child(2)', $row)); // glupo izgleda i useless e
                        $('#table-music-list').trigger('update'); // Important!!
                    });
                    $('#table-music-list').show();
                }
            });
        }
    });

    function setEditInPlace($el) {
        $el.editInPlace({
            callback: function (unused, enteredText) {
                return enteredText;
            },
            show_buttons: true
        });
    }

    var downArrow = true;
    $('#table-music-list th').on({
        mousedown: function () {
            $(this).css('color', 'lightgrey');
        },
        mouseup: function () {
            $(this).css('color', 'white');
            if (downArrow) {
                $('small', $(this)).html("▲");
                downArrow = false;
            }
            else {
                $('small', $(this)).html("▼");
                downArrow = true;
            }
        }
    });

});