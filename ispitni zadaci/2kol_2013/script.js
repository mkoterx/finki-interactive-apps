$(function () {

    $('#btn-search').on('click', function () {
        var searchTerm = $('#input-search').val();
        if (searchTerm != '')
            getResults(searchTerm);
    });

    function getResults(keyword) {
        jQuery.ajax({
            url: "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=10&order=relevance&q=" +
            keyword + "&type=video&videoCaption=closedCaption&key=AIzaSyAyvig5VkfPt_lBR4sFl-ajsULtgUHmTwA",
            dataType: 'json',
            success: function (response) {
                jQuery.each(response.items, function (index, video) {
                    var $li = $('<li></li>');
                    $li.attr('title', video.snippet.title);
                    $li.attr('description', video.snippet.description);
                    $li.attr('largeThumbUrl', video.snippet.thumbnails.high.url);
                    $li.append('<img src="' + video.snippet.thumbnails.default.url + '"/>');
                    $li.append('<h4 style="margin-top: 5px">' + video.snippet.title + '</h4>');
                    $('#res-list').append($li);
                    $('#fav-list').show();
                });
            }
        });
    }

    $('#res-list').sortable({
        connectWith: '#fav-list'
    });

    $('#fav-list').sortable({
        connectWith: '#res-list'
    });

    function fillDetailsView() {
        var title = current.attr('title');
        var thumbUrl = current.attr('largeThumbUrl');
        var desc = current.attr('description');
        $('#title').html('<span>Title: </span>' + title);
        $('#thumbnail').attr('src', thumbUrl);
        $('#desc').html('<span>Description:</span><br><br>' + desc);
    }

    var current;
    $('body').on('click', '#fav-list li', function () {
        current = $(this);
        fillDetailsView();
        $('#main-view').hide();
        $('#detais-view').show();
    });

    $('#btn-prev').on('click', function () {
        console.log(current.prev());
        if (current.prev().length == 0) {
            current = $('#fav-list li:last-child');
        }
        else
            current = current.prev();
        fillDetailsView();
    });

    $('#btn-next').on('click', function () {
        console.log(current.next());
        if (current.next().length == 0)
            current = $('#fav-list li:first-child');
        else
            current = current.next();
        fillDetailsView();
    });

    $('#btn-back').on('click', function () {
        $('#detais-view').hide();
        $('#main-view').show();
    });
});
