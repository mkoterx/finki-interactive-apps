$(function () {


    $('#menu img').on('click', function () {
        $('nav ul').slideToggle();
    });


    var limit = 50;
    var offset = 0;
    var category = '';

    $('nav ul li').on('click', function () {
        category = $(this).html();
        $('#items').empty();
        getDeals(category, offset, limit);
    });

    $(window).scroll(function(){
        if  ($(window).scrollTop() >= $(document).height() - $(window).height() - 200){
            offset+=limit;
            getDeals(category, offset, limit);
        }
    });

    function getDeals(category, offset, limit) {
        jQuery.ajax({
            url: "https://partner-api.groupon.com/deals.json?tsToken=US_AFF_0_987654_123456_0&division_id=" +
            category + "&offset=" + offset + "&limit=" + limit,
            dataType: 'jsonp',
            success: function (data) {
                jQuery.each(data.deals, function (index, deal) {
                    var $card = generateCard(deal);
                    $('#items').append($card);

                });
            },
            error: function () {

            }
        });
    }

    function generateCard(deal) {
        $articleCard = $('<article></article>');
        $articleCard.addClass('card-item');
        $imgPoster = $('<img class="deal-poster"/>');
        $imgPoster.attr('src', deal.largeImageUrl);
        $divDesc = $('<div></div>');
        $divDesc.addClass('container-item-desc');
        $aTitle = $('<a target="_blank"></a>');
        $aTitle.attr('href', deal.dealUrl);
        $aTitle.addClass('deal-title');
        $aTitle.html(deal.title);
        $divDates = $('<div></div>');
        $divDates.addClass('deal-dates');
        $labelFromDate = $('<label></label>');
        $labelFromDate.attr('title', 'from-date');
        $labelFromDate.html('<small>From:</small>' + ' ' + deal.startAt.substring(0, 10));
        $labelToDate = $('<label></label>');
        $labelToDate.attr('title', 'to-date');
        $labelToDate.html('<small>To:</small>' + ' ' + deal.endAt.substring(0, 10));
        console.log(deal.largeImageUrl);
        
        $divDesc.append($aTitle);
        $divDates.append($labelFromDate);
        $divDates.append($labelToDate);
        $divDesc.append($divDates);
        $aShare = $('<a id="ico-fb-share" target="_blank"><img src="img/fb.svg" alt="Share on Facebook"/></a>');
        $aShare.attr('href', "https://www.facebook.com/sharer/sharer.php?u=" + deal.dealUrl);
        $divDesc.append($aShare);
        $articleCard.append($imgPoster);
        $articleCard.append($divDesc);
        return $articleCard;
    }

});