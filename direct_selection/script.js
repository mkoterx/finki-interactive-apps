$(function () {


    $('tr').hover(function () {
            if (!$(this).hasClass('row-selected '))
                $(this).addClass("row-hovered");
        },
        function () {
            $(this).removeClass("row-hovered");
        }
    );

    $("td input[type='checkbox']").on("click", function () {
        var $tr = $(this).parent().parent();
        $tr.toggleClass('row-selected ');
        $tr.toggleClass('row-hovered');
    });

    $("td:last-child").on("click", function () {
        var $tr = $(this).parent();
        $tr.toggleClass('row-selected ');
        $tr.toggleClass('row-hovered');

        var $checkbox = $(this).prev().children().eq(0);
        if ($checkbox.prop('checked') == true)
            $checkbox.prop('checked', false);
        else
            $checkbox.prop('checked', true);
    });

    $('#btn-delete').on('click', function () {
        if ($('.row-selected').length == 0)
            alert('No mails selected');
        else
            $('.row-selected').hide();
    });

    $('#btn-restore').on('click', function () {
        var $rows = $('.row-selected');
        $rows.show();
        $rows.each(function () {
            $(this).find('input').prop('checked', false);
        })
        $rows.removeClass('row-selected');
    });

});